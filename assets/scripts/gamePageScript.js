let modal = document.getElementById("greetingModal");
let showUserName = document.getElementById("showUserName");
let okButton = document.getElementById("okButton");
let afterGreeting = document.querySelector(".afterGreeting");
let scoreNum = document.getElementById("scoreNum");
let difficultyLevel = window.localStorage.getItem("difficultyLevel");
const mainBgAudio = document.getElementById("mainBgAudio");
const mainBgAudioBtn = document.getElementById("mainBgAudioBtn");
mainBgAudio.autoplay = true;

mainBgAudioBtn.addEventListener("click", function () {
  if (mainBgAudio.paused) {
    mainBgAudio.play();
    mainBgAudioBtn.src = "../images/soundon.png";
    audioState = "played";
  } else {
    mainBgAudio.pause();
    mainBgAudioBtn.src = "../images/soundoff.png";
    audioState = "paused";
  }
});
document.getElementById("difficultyLevel").innerText = difficultyLevel;
/////////////////////////////
let countOfBirds = 0;
let bird = document.getElementById("bird");
let bird2 = document.getElementById("bird2");
let bird3 = document.getElementById("bird3");
let gunFire = document.getElementById("gunFire");
let gunFire2 = document.getElementById("gunFire2");
let gunFire3 = document.getElementById("gunFire3");
let difficultyLevelSpeed;
function closeModal() {
  modal.style.display = "none";
  document.body.style.cursor = "crosshair";
  afterGreeting.style.display = "block";
}
showUserName.innerHTML += ` ${localStorage.getItem("userName")}`;
okButton.onclick = closeModal;

if (difficultyLevel == "easy") {
  difficultyLevelSpeed = 10;
} else if (difficultyLevel == "hard") {
  difficultyLevelSpeed = 1;
}

console.log(difficultyLevelSpeed);
/*
bird move
*/

function flyMove(flybird) {
  let x = parseInt(flybird.style.left || 0);
  let y = parseInt(window.innerWidth || 0) - 100;
  let randomTop = Math.random() * window.innerHeight;
  let z = randomTop > window.innerHeight - 200 ? randomTop - 200 : randomTop;

  if (x >= y) {
    countOfBirds++;
    document.getElementById("completeGame").innerText =
      parseInt(document.getElementById("completeGame").innerText) - 5 + "%";
    document.getElementById("lives").innerText =
      parseInt(document.getElementById("lives").innerText) - 1;
    flybird.style.top = z < 100 ? z : z + 50 + "px";
    flybird.style.left = -100 + "px";
    if (countOfBirds > 20) {
      resetGame();
      showUserName.innerHTML = `you lose ${localStorage.getItem("userName")}`;
      modal.style.display = "block";
      afterGreeting.style.display = "none";
    }
  } else {
    flybird.style.left = parseInt(flybird.style.left || 0) + 1 + "px";
  }
}

let endFly = setInterval(() => flyMove(bird), difficultyLevelSpeed);
let endFly2 = setInterval(() => flyMove(bird2), difficultyLevelSpeed);
let endFly3 = setInterval(() => flyMove(bird3), difficultyLevelSpeed);
const originalBirdSrc = bird.src;
const originalBird2Src = bird2.src;
const originalBird3Src = bird3.src;

console.log();
function resetBirdPosition(birdElement, originalSrc) {
  let randomTop = Math.random() * window.innerHeight;
  let z = randomTop > window.innerHeight - 200 ? randomTop - 200 : randomTop;
  birdElement.style.top = z < 150 ? z : z + 100 + "px";
  birdElement.style.left = "0px";
  birdElement.src = originalSrc;
  setTimeout(() => {
    birdElement.style.display = "block";
    let newEndFlyInterval = setInterval(
      () => flyMove(birdElement),
      difficultyLevelSpeed
    );
    if (birdElement === bird) {
      endFly = newEndFlyInterval;
    } else if (birdElement === bird2) {
      endFly2 = newEndFlyInterval;
    } else if (birdElement === bird3) {
      endFly3 = newEndFlyInterval;
    }
  }, difficultyLevelSpeed);
}
function resetGame() {
  scoreNum.innerText = "0";
  bird.style.display = "block";
  bird2.style.display = "block";
  bird3.style.display = "block";
  resetBirdPosition(bird, originalBirdSrc);
  resetBirdPosition(bird2, originalBird2Src);
  resetBirdPosition(bird3, originalBird3Src);
  modal.style.display = "none";
  afterGreeting.style.display = "block";
  clearInterval(endFly);
  clearInterval(endFly2);
  clearInterval(endFly3);
  countOfBirds = 0;
  document.getElementById("completeGame").innerText = "100%";
  document.getElementById("lives").innerText = "20";

  // to make the birds fly faster after you win in the next round
  // endFly = setInterval(() => flyMove(bird), difficultyLevelSpeed);
  // endFly2 = setInterval(() => flyMove(bird2), difficultyLevelSpeed);
  // endFly3 = setInterval(() => flyMove(bird3), difficultyLevelSpeed);
}

function handleBirdClick(
  birdElement,
  endFlyInterval,
  gunFireElement,
  scoreAdd,
  originalSrc
) {
  let score = parseInt(scoreNum.innerText);
  scoreNum.innerText = score + scoreAdd;
  birdElement.src = "../images/feather.png";
  clearInterval(endFlyInterval);

  if (score >= 20) {
    showUserName.innerHTML = `you won ${localStorage.getItem("userName")}`;
    modal.style.display = "block";
    afterGreeting.style.display = "none";
    okButton.onclick = resetGame;
    return;
  }
  if (countOfBirds > 7) {
    showUserName.innerHTML = `you lose ${localStorage.getItem("userName")}`;
    modal.style.display = "block";
    afterGreeting.style.display = "none";
    resetGame;
    return;
  }

  setTimeout(() => {
    birdElement.style.display = "none";
    resetBirdPosition(birdElement, originalSrc);
  }, 1);
  gunFireElement.play();
}

bird.addEventListener("click", () => {
  handleBirdClick(bird, endFly, gunFire, 1, originalBirdSrc);
});
bird2.addEventListener("click", () => {
  handleBirdClick(bird2, endFly2, gunFire2, -5, originalBird2Src);
});
bird3.addEventListener("click", () => {
  handleBirdClick(bird3, endFly3, gunFire2, 10, originalBird3Src);
});
