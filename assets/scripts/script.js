const mainBgAudio = document.getElementById("mainBgAudio");
const mainBgAudioBtn = document.getElementById("mainBgAudioBtn");
const submitBtn = document.getElementById("submitBtn");
const inputName = document.getElementById("name");
const redirectLink = document.getElementById("redirectLink");
// console.log("check");
let userName;
let difficultyLevel;
let audioState;
mainBgAudio.autoplay = true;

mainBgAudioBtn.addEventListener("click", function () {
  if (mainBgAudio.paused) {
    mainBgAudio.play();
    mainBgAudioBtn.src = "assets/images/soundon.png";
    audioState = "played";
  } else {
    mainBgAudio.pause();
    mainBgAudioBtn.src = "assets/images/soundoff.png";
    audioState = "paused";
  }
});
const openFormButton = document.getElementById("openFormButton");
const closeFormButton = document.getElementById("closeForm");
const loginForm = document.getElementById("loginForm");

openFormButton.addEventListener("click", () => {
  loginForm.style.display = "block";
});

closeFormButton.addEventListener("click", () => {
  loginForm.style.display = "none";
});

redirectLink.addEventListener("click", () => {
  userName = inputName.value;
  difficultyLevel =
    document.querySelector('input[name="options"]:checked').value || "easy";
  window.localStorage.setItem("userName", userName);
  window.localStorage.setItem("difficultyLevel", difficultyLevel);
  window.localStorage.setItem("sound", audioState);
});
console.log(audioState);
